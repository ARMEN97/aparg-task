<?php
	ini_set('memory_limit', '512M');
	ini_set('max_execution_time', 0);

	if((isset($_REQUEST['procedure']) && isset($_REQUEST['value'])) || ($_REQUEST['procedure'] == 'export'))
		new ApargBot($_REQUEST['procedure'], $_REQUEST['value']);

	class ApargBot {
		private	$host	= '127.0.0.1';
		private $user	= 'root';
		private $pass	= '';
		private $db		= 'aparg';
		private $mysql;

		public function __construct($procedure, $value) {
			$this->setupConnection();
			switch(strtolower(trim($procedure))) {
				case 'index'  : $this->Index($value);	break;
				case 'search' : $this->Search($value);	break;
				case 'export' : $this->Export(); 		break;	
			}
		}

		public function __destruct() {
			$this->mysql->close();
		}

		public function Index($url) {
			$scanned	= [$url];

			$fx = function($link, $parent_id) use(&$fx, &$scanned, $url) {
				$HTML = $this->getPage($link);
				if($HTML == '') return;

				$HTML	= mb_convert_encoding($HTML, 'HTML-ENTITIES', 'UTF-8');
				$DOM	= new DOMDocument();
				$DOM->loadHTML($HTML, LIBXML_NOERROR);
				$title	= @$DOM->getElementsByTagName('title')->item(0)->textContent;
				$body	= @$DOM->saveHTML($DOM->getElementsByTagName('body')->item(0));
				$links	= @$DOM->getElementsByTagName('a');
				
				if($parent_id == 0) {
					$parent_id = $this->insertWebsite($url, $parent_id, $title, $body);
				}

				foreach($links as $link) {
					$link = $this->isInternal($link->getAttribute('href'), $url);
					if($link !== false && !in_array(trim($link), $scanned)) {
						$pid = $this->insertWebsite($link, $parent_id, $title, $body);
						$scanned[] = $link;
						$fx($link, $pid);
					}
				}

				unset($HTML, $body, $links, $title); 
			};

			$fx($url, 0);

			$this->Response([
				'indexed'	=> count($scanned)
			]);
		}

		public function Search($keyword) {
			$data = [];
			$keyword = mysqli_real_escape_string($this->mysql, $keyword);
			$SQL = "
				SELECT * FROM Websites
				Where Title LIKE '%{$keyword}%'
				UNION
				SELECT *FROM Websites
				Where Content LIKE '%{$keyword}%'
				LIMIT 100
			";
			if($result = $this->mysql->query($SQL)) {
				while ($row = $result->fetch_assoc()) {
					// highlight founded keyword
					$row['title']	= str_ireplace($keyword, "<span style='background-color: yellow; color: red; font-weight: 500;'>{$keyword}</span>", $row['title']);
					// $row['content']	= str_ireplace($keyword, "<span style='background-color: yellow; color: red; font-weight: 500;'>{$keyword}</span>", $row['content']);
					
					$data[] = $row;
				}
				$result->free();
			}

			$this->Response([
				'result'	=> $data
			]);
		}

		public function Export() {
			$node = null;
			$fx = function($parent_id, &$xml) use(&$fx, &$node) {
				$SQL = "SELECT * FROM Websites where `parent_id` = {$parent_id}";
				if($result = $this->mysql->query($SQL)) {
					while ($row = $result->fetch_assoc()) {
						$page = $xml->addChild("page");
						$page->addAttribute('id', $row['id']);
						$page->addAttribute('parent_id', $row['parent_id']);
						$page->addChild('title', htmlspecialchars($row['title']));
						$page->addChild('url', htmlspecialchars($row['url']));
						$page->addChild('content', $row['content']);
						$node = $page->addChild('pages');
						$fx($row['id'], $node);
					}
					$result->free();
				}
			};

			$xml = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
			$fx(0, $xml);

			header('Content-type: text/xml');
			echo $xml->asXML();
		}

		private function setupConnection() {
			$this->mysql = @new mysqli($this->host, $this->user, $this->pass, $this->db);

			if ($this->mysql->connect_error) {
				die('Connection failed: ' . $this->mysql->connect_error);
			} 
		}

		private function insertWebsite($url, $parent_id, $title, $content) {
			$content = mysqli_real_escape_string($this->mysql, $content);
			$SQL = "INSERT INTO Websites(`parent_id`, `url`, `title`, `content`) VALUES(
				{$parent_id},
				'{$url}',
				'{$title}',
				'{$content}'
			)";
			
			$this->mysql->query($SQL);
			return $this->mysql->insert_id;
		}

		private function getPage($url) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
			$result = curl_exec($curl);
			curl_close($curl);
			return $result;	
		}

		private function isInternal($link, $url) {
			$url_parts	= parse_url($url);
			$link_parts	= parse_url($link);
	
			if(isset($link_parts['host'])) {
				if($link_parts['host'] == $url_parts['host']) {
					if(substr($link, 0, 2) == '//') {
						return $url_parts['scheme'] . ':' . $link;
					} else {
						return $link;
					}
				}
			} else {
				if($link_parts['path'][0] == '/' && strlen($link_parts['path']) > 1) {
					return $url_parts['scheme'] . '://' . $url_parts['host'] . $link;
				}
			}
			return false;
		}

		private function Response($data) {
			echo json_encode($data, JSON_UNESCAPED_UNICODE);
		}
	}

	// print value for easy debugging :)
	function pv($data) {
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}
?>