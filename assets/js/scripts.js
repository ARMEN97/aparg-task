$(document).ready(function() {
	$(document)
		.on('click', '.form__button--index', function() {
			var value = $('.form__input--index').val().trim();
			if(value != '') {
				$('.form__input--index').removeClass('form__input--error');
				var data = {
					'procedure': 'index',
					'value': value
				};

				do_ajax('/process.php', data, function(data) {
					alert('Indexed: ' + data.indexed + ' pages');
				});
			} else {
				$('.form__input--index').addClass('form__input--error');
			}
		})
		.on('click', '.form__button--search', function() {
			var value = $('.form__input--search').val().trim();
			if(value != '') {
				$('.form__input--search').removeClass('form__input--error');
				var data = {
					'procedure': 'search',
					'value': value
				};

				do_ajax('/process.php', data, function(data) {
					$('.list').empty();
					$.each(data.result, function(i, el) {
						
						var item = $(`
							<div class='list__item'>
								<a href="` + el.url + `" alt="` + el.title + `" target="_blank" class="list__link">` + (el.title == '' ? 'Link' : el.title) + `</a>
								<button class='list__button form__button'>Show content</button>
								<div class='list__content'></div>
							</div>
						`);
						item.find('.list__content').text(el.content);
						$('.list').append(item);
					});
				});
			} else {
				$('.form__input--search').addClass('form__input--error');
			}
		})
		.on('click', '.form__button--export', function() {
			window.open('/process.php?procedure=export', '_blank');
		})
		.on('click', '.list__button', function() {
			var content = $(this).siblings('.list__content').toggleClass('list__content--active');
			$(this).text( content.hasClass('list__content--active') ? 'Hide content' : 'ShowContent');
		});
	;
});

function do_ajax(ajax_url, ajax_data, ajax_callback, ajax_async = true, ajax_type = 'json') {
	$.ajax({
		url	 	: ajax_url,
		type	: 'POST',
		dataType: ajax_type,
		async   : ajax_async,
		data	: ajax_data,
		success : ajax_callback
	});
}